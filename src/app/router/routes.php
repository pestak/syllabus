<?php

/**
 * liste des routes
 */

$router->add('/', function(){
    require_once APP . '/controller/default/index.php';
});

$router->add('/utilisateurs', function(){
    require_once APP . '/model/utilisateurs/test.php';
   
});
$router->add('/syllabus', function(){
    require_once APP . '/controller/syllabus/index.php';
   
});

$router->add('/taxonomies', function(){
    require_once APP . '/model/taxonomies/test.php';
});


$router->add('/pages', function(){
    require_once APP . '/controller/pages/index.php';
});

$router->add('/template', function(){
    require_once APP . '/view/template.php';
});

