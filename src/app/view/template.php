<?php



?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?> - Syllabus</title>
    <link rel="icon" type="image/x-icon" href="favicon.png">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://kit.fontawesome.com/bf0521ad64.js" crossorigin="anonymous"></script>
</head>

<body class="syllabus-partages">
    <header id="header">
        <div class="identite">
            <a href="index.php">
                <?= syllabus\HTML\EnteteSVG::retournerDirectement('images/logo.svg') ?>
            </a>
            <header>
                <h1 class="titre-site">Syllabus</h1>
                <div class="slogan">
                    Concevoir des séquences de formations dans un souci d'alignement pédagogique
                </div>
            </header>
        </div>
        <nav id="navigation" class="no-sm">
            <div id="icon-burger">
                    <?php 
                    $burger = new syllabus\HTML\EnteteSVG('images/burger-icon.svg');
                  
                    $burger->definirUneCouleur('#FFF');
                    
                    $burger->definirTitle('Menu');
                    echo $burger->rendre();
                    ?>
            </div>
            <ul class="menu menu-principal">

                <li class="item"><a href="index.php?controller=mes_syllabus">Mes syllabus</a></li>
                <!--li class="item"><a href="syllabus-partages.html">Syllabus partagés</a></li-->
                <li class="item"><a href="index.php?controller=taxonomie">Taxonomie de Bloom</a></li>
            </ul>
            <div class="perso">
                <img src="images/profil.png" alt="profil">
                <div class="nom">
                    <? echo $profil->prenom . ' ' . $profil->nom ?>
                </div>
            </div>
        </nav>
        <nav id="sous-menu-profil" class="est_invisible_sous_menu_profil">
            <ul class="menu menu-profil">
                <li class="item"><a href="index.php?controller=profil">Mon profil</a></li>
                <li class="item"><a href="index.php?controller=utilisateurs">Les utilisateurs</a></li>
                <li class="item"><a href="index.php?controoler=utilisateurs&action=deconnexion">Se déconnecter</a></li>
            </ul>
        </nav>
        <nav id="sous-menu-principal" class="est_invisible_sous_menu_principal">
            <ul class="menu">
            <li class="item"><a href="index.php?controller=mes_syllabus">Mes syllabus</a></li>
                <!--li class="item"><a href="syllabus-partages.html">Syllabus partagés</a></li-->
                <li class="item"><a href="index.php?controller=taxonomie">Taxonomie de Bloom</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <h2 class="titre-page"><?= $title ?></h2>
        <?= $content ?>

    </main>

    <footer id="footer">
        <?= syllabus\HTML\EnteteSVG::retournerDirectement('images/logo-footer.svg') ?>
        <nav class="navigation-footer">
            <ul class="menu menu-secondaire">
                <!--li class="item"><a href="inscription.html">Inscription-béta test</a></li-->
                <li class="item"><a href="index.php?controller=pages&action=contact">Contact</a></li>
                <li class="item"><a href="index.php?controller=pages&action=mentionlegales">Mentions légales</a></li>
                <!--li class="item"><a href="index.php?controller=pages&action=donnees">Données personnelles</a></li-->
            </ul>
            <div class="syllabus-footer">
                <a href="">Syllabus</a>
                <p>une application proposée par Les acteurs du Web</p>
            </div>
    </footer>
    <script src="js/navigation.js"></script>

</body>

</html>