<?php
use syllabus\BDD\BDD;
use syllabus\BDD\CRUD;
use syllabus\Outils\Debug;

/**
 * CivilitesBDD gère les utilisateurs
 */
class utilisateursBDD extends BDD implements CRUD{
    /**
     * lister : liste tous les utilisateurs
     * @param int $id : si non null on ne veut que celui la
     * @return array $utilisateurs : le/les enregistrements dans un/des tableau(x)
     */
    public function lister(int $id=null){
        $sql = 'SELECT *,  civilites.civilite, civilites.abrege FROM utilisateurs
        INNER JOIN civilites On civilites.id_civilites = utilisateurs.civilites_id';

        if(is_null($id)){
           return $this->bdd->query($sql)->fetchall(PDO::FETCH_ASSOC);
        }
        $sql .= ' WHERE id_utilisateurs=' . $id;
        return $this->bdd->query($sql)->fetch(PDO::FETCH_ASSOC);
       
    }

    /**
     * ajouter : ajouter un utilisateur
     * @param array $utilisateur : [str prenom=>NULL, str nom=>NULL, str login=>, str mail=>, str photo_profil=>NULL, str mot_de_passe=>, bool est_mail_verifie=>0, bool est_active=>0, int civilites_id =>1 ]
     * @return bool 
     */
    public function ajouter(array $data){
       

    

    }


    /**
     * supprimer : supprimer une civilite
     *
     * @param integer $id : l'id de la civilite à supprimer
     * @return bool 
     */
    public function supprimer(int $id){
        $q = $this->bdd->prepare('DELETE FROM civilites WHERE id_civilites=?');
        return $q->execute([$id]);
    }


    /**
     * modifier : modifie un civilite
     *
     * @param array $data : tableau de type ['civilite'=> <le civilite>, 'abrege'=>null 'id'=><id> ]
     * @return bool 
     */
    public function modifier(array $data){ // fonction
        $q = $this->bdd->prepare('UPDATE civilites SET civilites.civilite=:civilite, abrege=:abrege WHERE id_civilites=:id');
        return $q->execute([
            ':civilite'=>$data['civilite'],
            ':abrege'=>$data['abrege'],
            ':id'=>$data['id']
        ]);
    }

    
} 