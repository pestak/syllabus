<?php
use syllabus\BDD\BDD;
use syllabus\BDD\CRUD;



/**
 * RolesBDD gère la table roles
 */
class RolesBDD extends BDD implements CRUD{
    /**
     * lister : liste tous les roles 
     * @param int $id : si non null on ne veut que celui la
     * @return array $roles : le/les enregistrements dans un/des tableau(x)
     */
    public function lister(int $id=null){
        if(is_null($id)){
            $q = $this->bdd->query('SELECT * FROM roles');
            $roles = $q->fetchall(PDO::FETCH_ASSOC);
        }else{
            $q = $this->bdd->prepare('SELECT * FROM roles WHERE id_roles=?');
            $q-> execute([$id]);
            $roles = $q->fetch(PDO::FETCH_ASSOC);
        }
        return $roles;
    }

    /**
     * ajouter : ajouter un role
     * @param array $role : le nom du role (string) dans un tableau;
     * @return bool 
     */
    public function ajouter(array $data){
        $q = $this->bdd->prepare('INSERT INTO roles(roles.role) VALUES (:role)');
        $q->bindParam(':role', $data['role']);
        return $q->execute();
    }


    /**
     * supprimer : supprimer un role
     *
     * @param integer $id : l'id du role à supprimer
     * @return bool 
     */
    public function supprimer(int $id){
        $q = $this->bdd->prepare('DELETE FROM roles WHERE id_roles=?');
        return $q->execute([$id]);
    }


    /**
     * modifier : modifie un role
     *
     * @param array $data : tableau de type ['role'=> <le role>, 'id'=><id> ]
     * @return bool 
     */
    public function modifier(array $data){
        $q = $this->bdd->prepare('UPDATE roles SET roles.role=:role WHERE id_roles=:id');
        return $q->execute([
            ':role'=>$data['role'],
            ':id'=>$data['id']
        ]);
    }

    
} 