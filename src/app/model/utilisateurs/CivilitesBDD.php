<?php
use syllabus\BDD\BDD;
use syllabus\BDD\CRUD;



/**
 * CivilitesBDD gère la table civilites
 */
class CivilitesBDD extends BDD implements CRUD{
    /**
     * lister : liste tous les civilites 
     * @param int $id : si non null on ne veut que celui la
     * @return array $civilites : le/les enregistrements dans un/des tableau(x)
     */
    public function lister(int $id=null){
        if(is_null($id)){
            $q = $this->bdd->query('SELECT * FROM civilites');
            $civilites = $q->fetchall(PDO::FETCH_ASSOC);
        }else{
            $q = $this->bdd->prepare('SELECT * FROM civilites WHERE id_civilites=?');
            $q-> execute([$id]);
            $civilites = $q->fetch(PDO::FETCH_ASSOC);
        }
        return $civilites;
    }

    /**
     * ajouter : ajouter une civilité
     * @param array $civilite : ['civilite'=>, 'abrege'=>null];
     * @return bool 
     */
    public function ajouter(array $data){
        $q = $this->bdd->prepare('INSERT INTO civilites(civilite, abrege) VALUES (:civilite, :abrege)');
        $q->bindParam(':civilite', $data['civilite']);
        $q->bindParam(':abrege',$data['abrege']);
        return $q->execute();
    }


    /**
     * supprimer : supprimer une civilite
     *
     * @param integer $id : l'id de la civilite à supprimer
     * @return bool 
     */
    public function supprimer(int $id){
        $q = $this->bdd->prepare('DELETE FROM civilites WHERE id_civilites=?');
        return $q->execute([$id]);
    }


    /**
     * modifier : modifie un civilite
     *
     * @param array $data : tableau de type ['civilite'=> <le civilite>, 'abrege'=>null 'id'=><id> ]
     * @return bool 
     */
    public function modifier(array $data){ // fonction
        $q = $this->bdd->prepare('UPDATE civilites SET civilites.civilite=:civilite, abrege=:abrege WHERE id_civilites=:id');
        return $q->execute([
            ':civilite'=>$data['civilite'],
            ':abrege'=>$data['abrege'],
            ':id'=>$data['id']
        ]);
    }

    
} 