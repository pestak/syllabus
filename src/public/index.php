<?php

/**
 * index.php : entree de notre application
 */

use syllabus\Autoloader;
use syllabus\Outils\Debug;

require_once '../var/config.php';


require_once ABS_PATH .'/libs/autoloader.php';
Autoloader::register();

Debug::charge(Debug::DEBUT); //temps execution + memoire consommée


require_once APP . '/router/_router.php';

Debug::charge(Debug::FIN);
 
