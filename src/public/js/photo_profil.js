let imgElt = document.getElementById('photo-profil');


var previewPicture  = function (e) {

    // e.files contient un objet FileList
    const [picture] = e.files

    // "picture" est un objet File
    if (picture) {
        // On change l'URL de l'image
        imgElt.src = URL.createObjectURL(picture)
        imgElt.style.height = '200px';
        imgElt.style.width = '200px';
        
    }
} 
