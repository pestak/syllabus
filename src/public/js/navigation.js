/*=========================================================
SOUS MENU de profil
========================================================*/

let persoElt = document.querySelector('#navigation .perso');
let sousMenuProfilElt = document.getElementById('sous-menu-profil');

persoElt.addEventListener('click', function () {
    if (sousMenuProfilElt.classList.contains('est_invisible_sous_menu_profil')) {
        sousMenuProfilElt.classList.remove('est_invisible_sous_menu_profil');
    } else {
        sousMenuProfilElt.classList.add('est_invisible_sous_menu_profil');
    }
});

/*=========================================================
SOUS MENU principal
=========================================================*/

let iconElt = document.getElementById('icon-burger');
let sousMenuPrincipalElt = document.getElementById('sous-menu-principal');

iconElt.addEventListener('click', function () {
    if (sousMenuPrincipalElt.classList.contains('est_invisible_sous_menu_principal')) {
        sousMenuPrincipalElt.classList.remove('est_invisible_sous_menu_principal');
        sousMenuProfilElt.classList.add('est_invisible_sous_menu_profil');
        //iconElt.style.backgroundImage = "url('images/burger-ferme.svg')";

    } else {
        sousMenuPrincipalElt.classList.add('est_invisible_sous_menu_principal');

    }
});
