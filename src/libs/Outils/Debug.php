<?php

namespace syllabus\Outils;

use syllabus\Debug as SyllabusDebug;

/**
 * Debug : offre quelques fonctions de débug
 */
class Debug
{
    const DEBUT = 0;
    const FIN = 1;

    static $debut;


    /**
     * print_r : comme un print_r mais mieux affiché
     *
     * @param array $tab : le tableau à afficher
     * @return void
     */
    static function print_r(array $tab)
    {
        echo '<pre>';
        print_r($tab);
        echo '</pre>';
    }

    public static function charge($etape = Debug::DEBUT)
    {
        $time = microtime(true);

        if ($etape == Debug::DEBUT) {
            self::$debut = $time;
        } else {
            $new_time = microtime(true);
            $time = $new_time - self::$debut;

            function convert($size) // pompé sur php.net
            {
                $unit = array('o', 'Ko', 'Mo', 'Go', 'To', 'Po');
                return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
            }


            echo '<br>---------------------------------------------<br>';
            echo 'Durée du scipt : ' . number_format($time, 3) . ' secondes <br>';
            echo 'Mémoire utilisée : ' . convert(memory_get_usage()) ;
            echo '<br>---------------------------------------------<br>';
        }
    }
}
