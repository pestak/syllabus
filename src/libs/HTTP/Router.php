<?php

namespace syllabus\HTTP;


class Router
{
    private $requete;
    private $routes;
  


    public function __construct($uri)
    {
        $this->requete = '/' . $uri;
    }


    //le temps de debug
    public function __get($name)
    {
        return $this->$name;
    }

    public function add($route, $callback)
    {
        $this->routes[] = [$route, $callback];
       
    }

    public function match()
    {
        foreach($this->routes as $clef=>$route){
            if(in_array($this->requete, $route)) $index = $clef;
        }
       if(!isset($index)){
            $index = 0;
            $msg = [
                'code'=>'warning',
                'txt'=>'page non trouvée'
            ];
       }

        $route = $this->routes[$index];

        call_user_func($route[1]);  
        
    }
}
