<?php
namespace syllabus\BDD;

interface CRUD{
    public function ajouter(array $data);
    public function lister(int $id=null);
    public function supprimer(int $id);
    public function modifier(array $data);
}