<?php

namespace syllabus\BDD;
use \PDO;
use \Exception;
use syllabus\Outils\Debug;

/**
 * BDD : connexion à la base de données : implémente PDO
 */
class BDD{
    /**
     * @var PDO $bdd : pilote de la BDD
     */
    protected $bdd;
    public static $nbre_instances = 0; //pour explication singleton -> Delphine
 
  


    /**
     * __construct : définit l'attribut PDO $bdd s'il n'est pas encore defini
     *
     * @param string $sgbdr : actuellement que mysql
     * @param string $dns : 'host=<hote>;dbname=<nom_de_la_base_de_donnees>'
     * @param string $user
     * @param string $pass
     * @return void
     */
    public function __construct()
    {
        try{
            $options = [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE =>  PDO::ERRMODE_EXCEPTION
            ];

            $dns = DB_DRIVER . ':' . DB_DNS;
           
            $this->bdd = new PDO($dns, DB_USER, DB_PASS, $options);
            self::$nbre_instances++; //pour explication singleton -> Delphine
            
                
        }catch(Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }
    public  function retournerChampsObligatoires(string $table){
        $schema = $this->bdd->query('DESC '.$table)->fetchAll(PDO::FETCH_ASSOC);
        Debug::print_r($schema);
    }
}

