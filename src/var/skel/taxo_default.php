<?php

/**
 * tableau contenant les verbess et activites de taxonomie par default classé par niveaux de taxonomie
 */

$taxo_default =
    [
        'connaissance' => [
            'verbes' => [
                'associer',
                'définir',
                'décrire',
                'étiqueter',
                'lister',
                'mémoriser',
                'nommer',
                'ordonner',
                'identifier',
                'relier',
                'noter',
                'reproduire',

            ],
            'activités' => [
                'écoute',
                'lecture',
                'classement',
                'catégorisation',
                'dictionnaire',
                'internet',
                'discussion',
                'association',
                'recherche',
                'observation',
                'enquete',
                'médias',
                'tempête d\'idées',
                'mots croisées',
                'métaphore',

            ]
        ],
        'compréhension' => [
            'verbes' => [
                'classifier',
                'décrire',
                'discuter',
                'expliquer',
                'exprimer',
                'identifier',
                'traduire',
                'indiquer',
                'situer',
                'reconnaitre',
                'rapporter',
                'reformuler',
                'réviser',
                'choisir',

            ],
            'activités' => [
                'quête',
                'énigme',
                'analogie',
                'récit',
                'image',
                'portfolio',
                'diagramme',
                'conte',
                'débat',
                'discussion',
                'explication',
                'causalité',
                'résumé',
                'composition',
                'plan',
                'schéma',

            ]
        ],
        'application' => [
            'verbes' => [
                'appliquer',
                'choisir',
                'démontrer',
                'employer',
                'illustrer',
                'interpréter',
                'opérer',
                'pratiquer',
                'planifier',
                'schématiser',
                'résoudre',
                'utiliser',
                'écrire',

            ],
            'activités' => [
                'liste',
                'jeu de rôle',
                'simulation',
                'dessin',
                'manipulation',
                'démonstration',
                'étude de cas',
                'poème',
                'carte',
                'ligne du temps',
                'illustration',
                'questionnaire',
                'scénario',
                'recette',
                'prédiction',
                'méthode',

            ]
        ],
        'analyse' => [
            'verbes' => [
                'analyser',
                'estimer',
                'calculer',
                'catégoriser',
                'comparer',
                'contraster',
                'critiquer',
                'différencier',
                'discriminer',
                'distinguer',
                'examiner',
                'expérimenter',

            ],
            'activités' => [
                'syllogisme',
                'décomposition',
                'observation',
                'argumentation',
                'sondage',
                'règles',
                'force et faiblesse',
                'tableau',
                'modélisation',
                'étiquettage',
                'pour et contre',
                'graphique',

            ]
        ],
        'synthèse' => [
            'verbes' => [
                'arranger',
                'assembler',
                'collecter',
                'composer',
                'construire',
                'créer',
                'développer',
                'formuler',
                'gérer',
                'organiser',
                'planifier',
                'préparer',
                'concevoir',

            ],
            'activités' => [
                'rapport',
                'hyphothèse',
                'définition',
                'formule',
                'livre',
                'article',
                'graphique',
                'tableau',
                'règle',
                'jeu',
                'machine',
                'invention',
                'règles',
                'programme',
                'expérience',
            ]
        ],
        'évaluation' => [
            'verbes' => [
                'arranger',
                'argumenter',
                'évaluer',
                'rattacher',
                'choisir',
                'comparer',
                'critiquer',
                'estimer',
                'déduire',
                'prédire',
                'chiffrer',
                'distinguer',
                'recommander',
                'justifier',
            ],
            'activités' => [
                'notation',
                'analyse',
                'correction',
                'test',
                'annotation',
                'auto-évaluation',
                'recommandation',
                'commantaire',
                'comparaison',
                'discusion',
                'examem',
            ]
        ]

    ];



