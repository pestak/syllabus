

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;



-- --------------------------------------------------------

--
-- Structure de la table `association_utilisateurs_roles`
--

CREATE TABLE `association_utilisateurs_roles` (
  `association_utilisateurs_roles` int(11) NOT NULL,
  `utilisateurs_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id_categories` int(11) NOT NULL,
  `categorie` varchar(127) NOT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `categories_parentes_id` int(11) DEFAULT NULL,
  `utilisateurs_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `categories_parentes`
--

CREATE TABLE `categories_parentes` (
  `id_categories_parentes` int(11) NOT NULL,
  `categorie_parente` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `civilites`
--

CREATE TABLE `civilites` (
  `id_civilites` int(11) NOT NULL,
  `civilite` varchar(45) DEFAULT NULL,
  `abrege` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `civilites`
--

INSERT INTO `civilites` (`id_civilites`, `civilite`, `abrege`) VALUES
(2, 'Monsieur', 'M.'),
(3, 'Madame', 'M<sup>me</sup>'),
(1, NULL, 'NF'),
(4, 'Mademoiselle', 'M<sup>lle</sup>');

-- --------------------------------------------------------

--
-- Structure de la table `criteres`
--

CREATE TABLE `criteres` (
  `id_criteres` int(11) NOT NULL,
  `critere` varchar(255) NOT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `objectifs_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `objectifs`
--

CREATE TABLE `objectifs` (
  `id_objectifs` int(11) NOT NULL,
  `objectif` varchar(2048) NOT NULL,
  `titre` varchar(1024) DEFAULT NULL COMMENT 'titre activite',
  `activite` mediumtext DEFAULT NULL,
  `ordre` tinyint(4) DEFAULT NULL,
  `date_modification` datetime DEFAULT current_timestamp(),
  `syllabus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `ressources`
--

CREATE TABLE `ressources` (
  `id_ressources` int(11) NOT NULL,
  `etiquette` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `est_partage` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Partagé dans le syllabus',
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `syllabus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id_roles` int(11) NOT NULL,
  `role` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id_roles`, `role`, `date_modification`) VALUES
(1, 'administrateur', '2022-10-05 23:38:57'),
(2, 'formateur', '2022-10-05 23:38:57');

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE `sections` (
  `id_sections` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` mediumtext DEFAULT NULL,
  `ordre` tinyint(4) DEFAULT NULL COMMENT 'Dans de la section par rapport au autre',
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `syllabus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `syllabus`
--

CREATE TABLE `syllabus` (
  `id_syllabus` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `logo` varchar(2048) DEFAULT NULL,
  `est_publie` tinyint(4) NOT NULL DEFAULT 0,
  `auteur` varchar(255) DEFAULT NULL,
  `arbre` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Stockage du syllabus sous forme dun arbre' CHECK (json_valid(`arbre`)),
  `date_modification` varchar(45) NOT NULL DEFAULT current_timestamp(),
  `utilisateurs_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `taxo_elements`
--

CREATE TABLE `taxo_elements` (
  `id_taxo_elements` int(11) NOT NULL,
  `taxo_element` varchar(63) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `est_automatique` tinyint(4) NOT NULL DEFAULT 0,
  `utilisateurs_id` int(11) NOT NULL,
  `taxo_types_id` int(11) NOT NULL,
  `taxo_niveaux_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `taxo_niveaux`
--

CREATE TABLE `taxo_niveaux` (
  `id_taxo_niveaux` int(11) NOT NULL,
  `taxo_niveau` varchar(63) NOT NULL,
  `date_modification` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `taxo_niveaux`
--

INSERT INTO `taxo_niveaux` (`id_taxo_niveaux`, `taxo_niveau`, `date_modification`) VALUES
(1, 'connaissance', '2022-10-05 23:47:58'),
(2, 'compréhension', '2022-10-05 23:47:58'),
(3, 'application', '2022-10-05 23:47:58'),
(4, 'analyse', '2022-10-05 23:47:58'),
(5, 'synthèse ', '2022-10-05 23:47:58'),
(6, 'évaluation', '2022-10-05 23:47:58');

-- --------------------------------------------------------

--
-- Structure de la table `taxo_types`
--

CREATE TABLE `taxo_types` (
  `id_taxo_types` int(11) NOT NULL,
  `taxo_type` varchar(45) NOT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `taxo_types`
--

INSERT INTO `taxo_types` (`id_taxo_types`, `taxo_type`, `date_modification`) VALUES
(1, 'verbes', '2022-10-05 23:48:48'),
(2, 'activités', '2022-10-05 23:48:48');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id_utilisateurs` int(11) NOT NULL,
  `prenom` varchar(63) DEFAULT NULL,
  `nom` varchar(63) DEFAULT NULL,
  `login` varchar(63) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `photo_profil` varchar(1023) DEFAULT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `est_mail_verifie` tinyint(4) DEFAULT 0,
  `est_active` tinyint(4) DEFAULT 0,
  `jeton` varchar(255) DEFAULT NULL,
  `jeton_date` datetime DEFAULT NULL,
  `date_modification` datetime NOT NULL DEFAULT current_timestamp(),
  `civilites_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `association_utilisateurs_roles`
--
ALTER TABLE `association_utilisateurs_roles`
  ADD PRIMARY KEY (`association_utilisateurs_roles`),
  ADD KEY `roles_id` (`roles_id`),
  ADD KEY `utilisateurs_id` (`utilisateurs_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_categories`),
  ADD KEY `fk_categories_categories_parentes` (`categories_parentes_id`),
  ADD KEY `fk_categories_utilisateurs` (`utilisateurs_id`);

--
-- Index pour la table `categories_parentes`
--
ALTER TABLE `categories_parentes`
  ADD PRIMARY KEY (`id_categories_parentes`);

--
-- Index pour la table `civilites`
--
ALTER TABLE `civilites`
  ADD PRIMARY KEY (`id_civilites`),
  ADD UNIQUE KEY `civilite_UNIQUE` (`civilite`);

--
-- Index pour la table `criteres`
--
ALTER TABLE `criteres`
  ADD PRIMARY KEY (`id_criteres`),
  ADD KEY `fk_criteres_objectifs` (`objectifs_id`);

--
-- Index pour la table `objectifs`
--
ALTER TABLE `objectifs`
  ADD PRIMARY KEY (`id_objectifs`),
  ADD KEY `fk_objectifs_syllabus` (`syllabus_id`);

--
-- Index pour la table `ressources`
--
ALTER TABLE `ressources`
  ADD PRIMARY KEY (`id_ressources`),
  ADD UNIQUE KEY `etiquette_UNIQUE` (`etiquette`),
  ADD KEY `fk_ressources_syllabus` (`syllabus_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_roles`),
  ADD UNIQUE KEY `role_UNIQUE` (`role`);

--
-- Index pour la table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id_sections`),
  ADD KEY `fk_sections_syllabus` (`syllabus_id`);

--
-- Index pour la table `syllabus`
--
ALTER TABLE `syllabus`
  ADD PRIMARY KEY (`id_syllabus`),
  ADD KEY `fk_syllabus_utilisateurs` (`utilisateurs_id`);

--
-- Index pour la table `taxo_elements`
--
ALTER TABLE `taxo_elements`
  ADD PRIMARY KEY (`id_taxo_elements`),
  ADD KEY `fk_taxo_elements_utilisateurs` (`utilisateurs_id`),
  ADD KEY `fk_taxo_elements_taxo_types` (`taxo_types_id`),
  ADD KEY `fk_taxo_elements_taxo_niveaux` (`taxo_niveaux_id`);

--
-- Index pour la table `taxo_niveaux`
--
ALTER TABLE `taxo_niveaux`
  ADD PRIMARY KEY (`id_taxo_niveaux`),
  ADD UNIQUE KEY `taxo_niveau_UNIQUE` (`taxo_niveau`);

--
-- Index pour la table `taxo_types`
--
ALTER TABLE `taxo_types`
  ADD PRIMARY KEY (`id_taxo_types`),
  ADD UNIQUE KEY `taxo_type_UNIQUE` (`taxo_type`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id_utilisateurs`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`),
  ADD UNIQUE KEY `mail_UNIQUE` (`mail`),
  ADD KEY `civilites_id` (`civilites_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `association_utilisateurs_roles`
--
ALTER TABLE `association_utilisateurs_roles`
  MODIFY `association_utilisateurs_roles` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_categories` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `categories_parentes`
--
ALTER TABLE `categories_parentes`
  MODIFY `id_categories_parentes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `civilites`
--
ALTER TABLE `civilites`
  MODIFY `id_civilites` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `criteres`
--
ALTER TABLE `criteres`
  MODIFY `id_criteres` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `objectifs`
--
ALTER TABLE `objectifs`
  MODIFY `id_objectifs` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ressources`
--
ALTER TABLE `ressources`
  MODIFY `id_ressources` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_roles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `sections`
--
ALTER TABLE `sections`
  MODIFY `id_sections` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `syllabus`
--
ALTER TABLE `syllabus`
  MODIFY `id_syllabus` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `taxo_elements`
--
ALTER TABLE `taxo_elements`
  MODIFY `id_taxo_elements` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `taxo_niveaux`
--
ALTER TABLE `taxo_niveaux`
  MODIFY `id_taxo_niveaux` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `taxo_types`
--
ALTER TABLE `taxo_types`
  MODIFY `id_taxo_types` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id_utilisateurs` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `association_utilisateurs_roles`
--
ALTER TABLE `association_utilisateurs_roles`
  ADD CONSTRAINT `fk_association_utilisateurs_roles_roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id_roles`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_association_utilisateurs_roles_utilisateurs` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id_utilisateurs`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `fk_categories_categories_parentes` FOREIGN KEY (`categories_parentes_id`) REFERENCES `categories_parentes` (`id_categories_parentes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_categories_utilisateurs` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id_utilisateurs`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `criteres`
--
ALTER TABLE `criteres`
  ADD CONSTRAINT `fk_criteres_objectifs` FOREIGN KEY (`objectifs_id`) REFERENCES `objectifs` (`id_objectifs`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `objectifs`
--
ALTER TABLE `objectifs`
  ADD CONSTRAINT `fk_objectifs_syllabus` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id_syllabus`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ressources`
--
ALTER TABLE `ressources`
  ADD CONSTRAINT `fk_ressources_syllabus` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id_syllabus`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `fk_sections_syllabus` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id_syllabus`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `syllabus`
--
ALTER TABLE `syllabus`
  ADD CONSTRAINT `fk_syllabus_utilisateurs` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id_utilisateurs`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `taxo_elements`
--
ALTER TABLE `taxo_elements`
  ADD CONSTRAINT `fk_taxo_elements_taxo_niveaux` FOREIGN KEY (`taxo_niveaux_id`) REFERENCES `taxo_niveaux` (`id_taxo_niveaux`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_taxo_elements_taxo_types` FOREIGN KEY (`taxo_types_id`) REFERENCES `taxo_types` (`id_taxo_types`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_taxo_elements_utilisateurs` FOREIGN KEY (`utilisateurs_id`) REFERENCES `utilisateurs` (`id_utilisateurs`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `fk_utilisateurs_civilites` FOREIGN KEY (`civilites_id`) REFERENCES `civilites` (`id_civilites`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
