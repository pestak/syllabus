<?php
/**
 * inc/config - definition des constantes
 */


 //la bdd
define('DB_DNS', 'host=localhost;dbname=syllabus');
define('DB_DRIVER', 'mysql');
define('DB_USER', 'root');
define('DB_PASS', '');

//quelques PATH
$abs_path = dirname(__DIR__);
$abs_path = str_replace('\\','/',$abs_path);
define('ABS_PATH', $abs_path);
define('APP', ABS_PATH.'/app');
define('LIBS', ABS_PATH . '/libs');
define('PUBLIQUE', ABS_PATH . '/public');
define('SKEL', ABS_PATH . '/var/skel');



$script = explode('/', $_SERVER['SCRIPT_NAME']);
$script = array_pop($script);




