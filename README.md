# Syllabus
Application pour faciliter la création de syllabus en favorisant l'alignement pédagogique.
Projet de stage TPDev CCIOpenLab 2022 [cci Charente formation](https://www.ccicharente-formation.fr/cci-creative/cci-open-lab/)

## Auteurs
- Delphine Raoux (stagiaire)
- David Legrand (maître de stage)

## Licence
Licence libre GPL