-- SQLite
--UPDATE recipes SET title='soupe de légumes' WHERE title="soupe";
--SELECT * FROM recipes; 
--ALTER TABLE recipes ADD id INT AUTO_INCREMENT;

--DROP TABLE recipes;
/*CREATE TABLE recipes (
  id INTEGER PRIMARY KEY NOT NULL, 
  title VARCHAR(150) NOT NULL,
  slug VARCHAR(150) NOT NULL,
  date DATETIME DEFAULT CURRENT_TIMESTAMP,
 duration INTEGER DEFAULT 0 NOT NULL
 );
 */

--CREATE UNIQUE INDEX idx_slug ON recipes(slug);
--DELETE FROM recipes WHERE id>=6;

--EXPLAIN QUERY PLAN SELECT * FROM recipes;

/*
INSERT INTO recipes(title, slug, duration)
VALUES
('Bolo','bolo', 20),
('Carbo','carbo', 20),
('Lasagnes', 'lasagnes', 80);
*/

--EXPLAIN QUERY PLAN SELECT * FROM recipes;
--EXPLAIN QUERY PLAN SELECT * FROM recipes WHERE title='Soupe de Légumes';
--EXPLAIN QUERY PLAN SELECT * FROM recipes WHERE id=1;





-- CREATE UNIQUE INDEX idx_slug ON recipes (slug);

CREATE TABLE IF NOT EXISTS categories(
    id INT PRIMARY KEY NOT NULL,
    title VARCHAR(65) UNIQUE NOT NULL,
    description TEXT DEFAULT NULL 
);

ALTER TABLE categories ADD AUTO_INCREMENT ON categories (id);

INSERT INTO categories(title) VALUES 
('dessert'),
('pate'),
('soupe');


SELECT * FROM categories;