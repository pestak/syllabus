let oeilElt = document.querySelector('img#oeil');
let inputPassWordElt = document.querySelector('[type="password"]');


oeilElt.addEventListener('click', function () {
    if (oeilElt.classList.contains('clique')) {
        oeilElt.classList.remove('clique');
        inputPassWordElt.setAttribute('type', 'password');
    } else {
        oeilElt.classList.add('clique');
        inputPassWordElt.setAttribute('type', 'text');
    }
});

